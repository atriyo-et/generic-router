<?php

namespace App\Helper;

class UrlHelper
{
    public static function UserLogin(array $apiServers): string
    {
        return $apiServers['user_management_api'].'/api/login_check';
    }
}