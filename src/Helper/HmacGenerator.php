<?php

namespace App\Helper;

class HmacGenerator
{
    public static function generateHmac(string $timestamp, string $requester, string $refreshToken, string $secretKey): string
    {
        $string = $timestamp.$requester.$refreshToken;

        return hash_hmac('sha256', $string, $secretKey);
    }
}