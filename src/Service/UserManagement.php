<?php

namespace App\Service;

use App\Helper\UrlHelper;
use App\Helper\HmacGenerator;
use Symfony\Component\HttpClient\CurlHttpClient;

class UserManagement
{
    public function __construct(private array $apiServers, private string $authUrl, private string $authExt, private string $secretKey, private UrlHelper $urlHelper)
    {
    }

    public function verifyCredentials($username, $password)
    {
        $curlHttpClient = new CurlHttpClient();
        $response = $curlHttpClient->request('POST', $this->urlHelper->getAuthUrl($this->apiServers), [
            'json' => ['username' => $username, 'password' => $password],
        ]);

        return $response->getContent();
    }

    public function requestTokens($token)
    {
        $curlHttpClient = new CurlHttpClient();
        $response = $curlHttpClient->request('GET', $this->urlHelper->getGenerateTokensUrl($this->apiServers, $token));

        return $response->getContent();
    }

    public function regenerateAccessToken($timestamp, $requester, $refreshToken)
    {
        $secretKey =$this->secretKey;
        $curlHttpClient = new CurlHttpClient();
        $message = HmacGenerator::generateHmac($timestamp, $requester, $refreshToken, $secretKey);
        $response = $curlHttpClient->request('POST', $this->urlHelper->getAccessTokenUrl($this->apiServers), [
            'headers' => [
                'MESSAGE' => $message,
                'TIMESTAMP' => $timestamp,
                'REQUESTER' => $requester
            ],
            'json' => ['refresh_token' => $refreshToken],
        ]);
        return $response->getContent();
    }

    public function requestLogout($accessToken, $refreshToken)
    {
        $curlHttpClient = new CurlHttpClient();
        $response = $curlHttpClient->request('POST', $this->urlHelper->getLogoutUrl($this->apiServers), [
            'headers' => [
                'Authorization' => 'Bearer '.$accessToken,
            ],
            'json' => ['refresh_token' => $refreshToken],
        ]);
        return $response->getContent();
    }
}
